package imgbb

import (
	"bytes"
	"encoding/json"
	"github.com/gofiber/fiber"
	"io/ioutil"
	"mime/multipart"
	"net/http"
)

func apiUploadImage(c *context, name string, image string, response *ResponseUploadImgBB) error {
	url := "https://api.imgbb.com/1/upload?&key=" + c.apiKey + "&name" + name

	payload := &bytes.Buffer{}
	writer := multipart.NewWriter(payload)
	_ = writer.WriteField("image", image)
	err := writer.Close()
	if err != nil {
		return err
	}

	client := &http.Client{}
	req, err := http.NewRequest(fiber.MethodPost, url, payload)

	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", writer.FormDataContentType())
	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}
	if err := json.Unmarshal(body, &response)
	err != nil {
		return err
	}

	//if resRequest.Code != 200 {
	//	return fmt.Errorf(string(resRequest.Result))
	//}
	//if err := json.Unmarshal(resRequest.Result, &response); err != nil {
	//	return err
	//}
	return nil
}
