package imgbb

type Thumb struct {
	Filename  string `json:"filename"`
	Name      string `json:"name"`
	Mime      string `json:"mime"`
	Extension string `json:"extension"`
	Url       string `json:"url"`
}

type Image struct {
	Filename  string `json:"filename"`
	Name      string `json:"name"`
	Mime      string `json:"mime"`
	Extension string `json:"extension"`
	Url       string `json:"url"`
}

type Data struct {
	Id         string `json:"id"`
	Title      string `json:"title"`
	UrlViewer  string `json:"url_viewer"`
	Url        string `json:"url"`
	DisplayUrl string `json:"display_url"`
	Width      string `json:"width"`
	Height     string `json:"height"`
	Size       int    `json:"size"`
	Time       string `json:"time"`
	Expiration string `json:"expiration"`
	Image      Image  `json:"image"`
	Thumb      Thumb  `json:"thumb"`
	DeleteUrl  string `json:"delete_url"`
}

type ResponseUploadImgBB struct {
	Data    Data `json:"data"`
	Success bool `json:"success"`
	Status  int  `json:"status"`
}
