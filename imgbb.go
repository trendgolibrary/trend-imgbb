package imgbb

type (
	Context interface {
		Error() error
		UploadImage(name string, imgBase64 string, response *ResponseUploadImgBB) Context
	}
	context struct {
		apiKey string
		err    error
	}
)

func (c *context) UploadImage(name string, imgBase64 string, response *ResponseUploadImgBB) Context {
	err := apiUploadImage(c, name, imgBase64, response)
	if err != nil {
		c.err = err
		return c
	}
	c.err = nil
	return c
}

func (c *context) Error() error {
	if c.err != nil {
		return c.err
	}
	return nil
}
